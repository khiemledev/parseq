.PHONY: train
train_pretrained:
	 python3 train.py\
	 	pretrained=parseq\
	 	charset=224_vn_full\
		dataset=real\
		model.batch_size=42\
		data.augment=true\
		data.num_workers=8\
		data.root_dir=data\
		trainer.max_epochs=50\
		trainer.gpus=1\
		+trainer.accelerator=gpu

.PHONY: train_scratch
train_scratch:
	python3 train.py\
	 	charset=224_vn_full\
		dataset=real\
		model.batch_size=42\
		data.augment=true\
		data.num_workers=8\
		data.root_dir=data\
		trainer.max_epochs=100\
		trainer.gpus=1\
		+trainer.accelerator=gpu

.PHONY: resume
resume:
	python3 train.py\
	 	charset=224_vn_full\
		dataset=real\
		model.batch_size=42\
		data.augment=true\
		data.num_workers=8\
		data.root_dir=data\
		trainer.max_epochs=50\
		trainer.gpus=1\
		+trainer.accelerator=gpu\
		ckpt_path=outputs/parseq/2022-12-04_17-07-02/checkpoints/last.ckpt

.PHONY: custom_eval
custom_eval:
	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/VinText_artonly_cropped\
		--label_file all.txt

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/BKAI_TEXT_artonly_cropped\
		--label_file all.txt

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/VietSceneText_artonly_cropped\
		--label_file all.txt

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/CUTE80_artonly_cropped\
		--label_file all.txt

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/TotalText_artonly_cropped\
		--label_file all.txt

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/IC13_artonly_cropped\
		--label_file all.txt

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/WordArt\
		--label_file test_label.txt\
		--split_char " "

.PHONY: custom_eval
custom_eval_rm_accent:
	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/VinText_artonly_cropped\
		--label_file all_rm_accent.txt --without_accent

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/BKAI_TEXT_artonly_cropped\
		--label_file all_rm_accent.txt --without_accent

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/VietSceneText_artonly_cropped\
		--label_file all_rm_accent.txt --without_accent

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/CUTE80_artonly_cropped\
		--label_file all_rm_accent.txt --without_accent

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/TotalText_artonly_cropped\
		--label_file all_rm_accent.txt --without_accent

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/IC13_artonly_cropped\
		--label_file all_rm_accent.txt --without_accent

	python3 custom_inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/WordArt\
		--label_file test_label.txt\
		--split_char " "\
		--without_accent


.PHONY: eval
eval:
	python3 test.py\
		/mlcv/WorkingSpace/SceneText/khiemltt/GraduateThesis/source/parseq/outputs/parseq/2022-12-04_17-07-02/checkpoints/last.ckpt\
		--data_root=data\
		--batch_size=42\
		--num_workers=8\
		--device cuda


.PHONY: fps
fps:
	python3 custom_eval.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../dataset/CUTE80_artonly_cropped\
		--label_file all.txt --measure_fps

.PHONY: infer
infer:
	python3 inference.py ./outputs/parseq/parseq_vnfull_100ep/checkpoints/last.ckpt\
		--img_dir ../../test_images\
		--label_file result.csv
