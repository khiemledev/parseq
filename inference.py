import argparse
from strhub.models.utils import load_from_checkpoint, parse_model_args
from pathlib import Path
from strhub.data.module import SceneTextDataModule
from PIL import Image
from ocr_metric import eval_ocr_metric
from tqdm import tqdm
from unidecode import unidecode
import torch
import time
import pandas as pd


def eval(model, img_transform, args):
    root = Path(args.img_dir)
    label_file = root / args.label_file
    total_time = 0

    with label_file.open("r", encoding="utf-8") as f:
        labels = []
        preds = []
        lines = f.readlines()
        lines = list(map(lambda x: x.strip().split(args.split_char), lines))
        lines = list(filter(lambda x: len(x) == 2, lines))

        for i in tqdm(range(len(lines) // args.batch_size + 1)):
            data = lines[i * args.batch_size: (i + 1) * args.batch_size]

            # Load images into batch
            imgs = []
            _labels = []
            tic = time.time()
            for path, text in data:
                img_path = root / path
                img = Image.open(img_path).convert('RGB')
                img = img_transform(img)
                # img = img.to(args.device)
                imgs.append(img)
                _labels.append(text)
            if len(imgs) == 0:
                continue
            imgs = torch.stack(imgs)
            imgs = imgs.to(args.device)

            with torch.no_grad():
                logits = model(imgs)
                pred = logits.softmax(-1)
                pred_strs, confidence = model.tokenizer.decode(pred)

            for pred_text, text in zip(pred_strs, _labels):
                if args.without_accent:
                    text = unidecode(text)
                    pred_text = unidecode(pred_text)

                labels.append(text)
                preds.append(pred_text)

            toc = time.time()
            total_time += toc - tic

        res = eval_ocr_metric(preds, labels, metric='acc')
        total_images = len(labels)
        if not args.measure_fps:
            print(res)
        else:
            print(f"Processing time: {total_time}")
            print(f"No.images: {total_images}")
            print(f"FPS: {total_images / total_time:.2f}")
        return res, total_images, total_time


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('checkpoint', help="Model checkpoint (or 'pretrained=<model_id>')")
    parser.add_argument('--num_workers', type=int, default=4)
    parser.add_argument('--cased', action='store_true', default=False, help='Cased comparison')
    parser.add_argument('--punctuation', action='store_true', default=False, help='Check punctuation')
    parser.add_argument('--new', action='store_true', default=False, help='Evaluate on new benchmark datasets')
    parser.add_argument('--rotation', type=int, default=0, help='Angle of rotation (counter clockwise) in degrees.')
    parser.add_argument('--device', default='cuda')
    parser.add_argument('--custom', action='store_true', default=True, help='Evaluate on custom dataset')
    parser.add_argument('--img_dir', type=str, required=True, help='Path to image folder')
    parser.add_argument('--label_file', type=str, required=True, help='Path to label file')
    args, unknown = parser.parse_known_args()
    kwargs = parse_model_args(unknown)

    model = load_from_checkpoint(args.checkpoint, **kwargs).eval().to(args.device)
    img_transform = SceneTextDataModule.get_transform(model.hparams.img_size, augment=False)

    root_dir = Path(args.img_dir)
    label_file = root_dir / args.label_file
    df = pd.read_csv(label_file)

    preds = []

    with torch.no_grad():
        for i, row in df.iterrows():
            img_path = root_dir / row['file_name']
            img = Image.open(img_path)
            imgs = img_transform(img).unsqueeze(0).to(args.device)

            logits = model(imgs)
            pred = logits.softmax(-1)
            pred_strs, confidence = model.tokenizer.decode(pred)
            text = pred_strs[0]

            preds.append(text.upper())

    df['PARSeq'] = preds

    cols = [
        'file_name',
        'label',
        'ViTSTR',
        'SATRN',
        'CornerTransformer',
        'VietOCR',
        'ABINet',
        'PARSeq',
    ]

    df.to_csv('../../test_images/result.csv',
              index=None, header=True, columns=cols,)


if __name__ == "__main__":
    main()
